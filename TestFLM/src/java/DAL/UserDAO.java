/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.DBcontext;
import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author hp
 */
public class UserDAO extends DBcontext {

    //khai bao cac thanh phan xu li DB
    Connection cnn; //ket noi db
    Statement stm; // Thuc thi cau lenh sql
    PreparedStatement pstm; //ban nang cap cua stm
    ResultSet rs; //Luu tru va xu ly du lieu

    public User getUserProfile(String user_id) {
        User u = new User();
        try {
            //step 1:
            cnn = DBcontext.getJDBCConnect();
            //Step 2:
            String sql = "select * from user where user_id=? ";
            //step 3:
            pstm = cnn.prepareStatement(sql);           
            pstm.setString(1, user_id);
            //step 4:        
            rs = pstm.executeQuery();
            while (rs.next()) {
                u.setUser_id(rs.getString(1));
                u.setFull_name(rs.getString(2));
                u.setEmail(rs.getString(3));
                u.setMobile(rs.getString(4));
                u.setUser_name(rs.getString(5));
                u.setPassword(rs.getString(6));
                u.setStatus(rs.getString(7));
            }
//            return u;
//            System.out.println(u.getUser_id());
        } catch (Exception e) {
            System.out.println("getUserProfile" + e.getMessage());
        }
        return u;
    }

    public ArrayList<User> getListUser() {
        ArrayList<User> data = new ArrayList<>();
        try {
            Connection cnn = DBcontext.getJDBCConnect();
            Statement stm = cnn.createStatement();

            String strSelect = "select *  from user ";
            ResultSet rs = stm.executeQuery(strSelect);

            while (rs.next()) {
                String user_id = rs.getString(1);
                String full_name = rs.getString(2);
                String email = rs.getString(3);
                String mobile = rs.getString(4);
                String user_name = rs.getString(5);
                String password = rs.getString(6);
                String status = rs.getString(7);

                data.add(new User(user_id, full_name, email, mobile, user_name, password, status));

            }
        } catch (Exception e) {
            System.out.println("getListUser()" + e.getMessage());
        }
        return data;

    }
    
    
    
    public static void main(String[] args) {
        
    }

}
