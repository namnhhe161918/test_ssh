/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import DAL.UserDAO;
import Model.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hp
 */
public class ListUserController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user_id = req.getParameter("uesr_id");
//        resp.getWriter().print(user_id);
       
//        if (sb.equals("submit")) {           
            User user = new User();
            UserDAO u = new UserDAO();
            u.getUserProfile(user_id);
//            System.out.println(u.getAccountStudent(user_id).getFull_name());
            req.setAttribute("u", u.getUserProfile(user_id));
            req.getRequestDispatcher("View/ListUser.jsp").forward(req, resp);
//        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDAO u = new UserDAO();
        ArrayList<User> data;
        data = u.getListUser();
        req.setAttribute("data", data);
        req.getRequestDispatcher("View/ListUser.jsp").forward(req, resp);
    }

    
    
}
