/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author hp
 */
public class DBcontext {

    public static Connection getJDBCConnect() {
        String url = "jdbc:mysql://localhost:3306/flm_database";
        String user = "root";
        String password = "12345678";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection(url, user, password);
        } catch (Exception e) {

        }
        return null;
    }

    public static void main(String[] args) {
        //step 1: create connect
        Connection connect = getJDBCConnect();
        if (connect != null) {
            System.out.println(connect);
            System.out.println("ok");
        } else {
            System.out.println("not ok");
        }

//        try {
//        //step 2: create opp statement
//        Statement stm = connect.createStatement(); // Thuc thi cau lenh sql
//        //step 3: Execute requirement sql
//        String sql = "insert into role(role_id, role_name) values (3,'reviewer')";    
//        int check = stm.executeUpdate(sql);
//        //step 4: solution impact
//        System.out.println("so dong thay doi " + check);
//        if (check > 0 ){
//            System.out.println("them thanh cong");
//        } else {
//            System.out.println("them that bai");
//        }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

}
